/**
 * 
 */
package him.page;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

/**
 * @author himanshu
 *
 */
@Service
public class ContextAnalysisService {

	public HtmlPage analyse(Map<String, String> urlHtmls) {
		Map<String, HtmlPage> urlPages = getHtmlPages(urlHtmls);
		HtmlPage analysedPage = BoilerPlateAnalyser.analyse(urlPages);
		CommentAnalyser.analyse(analysedPage);
		return analysedPage;
	}

	/**
	 * @param urlHtmls
	 */
	private Map<String, HtmlPage> getHtmlPages(Map<String, String> urlHtmls) {
		Map<String, HtmlPage> urlPages = new LinkedHashMap<String, HtmlPage>();
		for (Entry<String, String> urlHtmlPair : urlHtmls.entrySet()) {
			HtmlPage page = new HtmlPage(urlHtmlPair.getKey(), urlHtmlPair.getValue());
			Document dom = Jsoup.parse(page.getHtml(), page.getUrl());
			page.setDom(dom);
			urlPages.put(page.getUrl(), page);
		}
		return urlPages;
	}

}

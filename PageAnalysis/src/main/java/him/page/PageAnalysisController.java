package him.page;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PageAnalysisController {
	@Autowired
	private HtmlFetchService htmlFetchService;
	
	@Autowired
	private ContextAnalysisService contextAnalysisService;
	
	@RequestMapping(value="/page/analyse/tag", params="urls")
	public String tag(@RequestParam List<String> urls) throws HttpException, IOException {
		Map<String, String> urlHtmls = new LinkedHashMap<String, String>();
		for (String url : urls) {
			urlHtmls.put(url, htmlFetchService.fetchHtml(url));
		}
		HtmlPage analysedPage = contextAnalysisService.analyse(urlHtmls);
		analysedPage.tagDOM();
		return analysedPage.getDom().html();
	}
	
	@RequestMapping(value="/page/analyse/diff", params="urls")
	public String diff(@RequestParam List<String> urls) throws HttpException, IOException {
		Map<String, String> urlHtmls = new LinkedHashMap<String, String>();
		for (String url : urls) {
			urlHtmls.put(url, htmlFetchService.fetchHtml(url));
		}
		HtmlPage analysedPage = contextAnalysisService.analyse(urlHtmls);
		return analysedPage.getUncommonDocument().html();
	}
	
	@RequestMapping(value="/page/analyse/diff-text", params="urls")
	public String diffText(@RequestParam List<String> urls) throws HttpException, IOException {
		Map<String, String> urlHtmls = new LinkedHashMap<String, String>();
		for (String url : urls) {
			urlHtmls.put(url, htmlFetchService.fetchHtml(url));
		}
		HtmlPage analysedPage = contextAnalysisService.analyse(urlHtmls);
		return analysedPage.getUncommonTextDocument().html();
	}
}

/**
 * 
 */
package him.page;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jsoup.nodes.Document;

/**
 * @author himanshu
 *
 */
public class BoilerPlateAnalyser {

	public static HtmlPage analyse(Map<String, HtmlPage> urlPages) {
		HtmlPage firstPage = urlPages.values().iterator().next();
		List<Document> doms = getDOMs(urlPages);
		BoilerPlateFinder commonDocumentVisitor = new BoilerPlateFinder(doms.subList(1, doms.size()));
		firstPage.getDom().traverse(commonDocumentVisitor);
		firstPage.setCommonElements(commonDocumentVisitor.getCommonElements());
		firstPage.setUncommonElements(commonDocumentVisitor.getUncommonElements());
		firstPage.setOrderedElements(commonDocumentVisitor.getOrderedElements());
		return firstPage;
	}

	private static List<Document> getDOMs(Map<String, HtmlPage> urlPages) {
		List<Document> doms = new ArrayList<Document>();
		for (HtmlPage page : urlPages.values()) {
			doms.add(page.getDom());
		}
		return doms;
	}
	
}

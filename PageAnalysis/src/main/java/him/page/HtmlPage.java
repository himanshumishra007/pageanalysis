/**
 * 
 */
package him.page;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.parser.Tag;

/**
 * @author himanshu
 *
 */
public class HtmlPage {
	private String url;
	private String html;
	private Document dom;
	private List<Element> commonElements;
	private List<Element> uncommonElements;
	private List<Element> commentElements;
	private List<Element> orderedElements;
	private Integer commentsCountExact;
	private Integer commentsCountMin;
	private Integer commentsCountMax;
	
	public HtmlPage(String url, String html) {
		this.url = url;
		this.html = html;
		commentElements = new ArrayList<Element>();
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public Document getDom() {
		return dom;
	}
	public void setDom(Document dom) {
		this.dom = dom;
	}

	public List<Element> getCommonElements() {
		return commonElements;
	}

	public void setCommonElements(List<Element> commonElements) {
		this.commonElements = commonElements;
	}

	public List<Element> getUncommonElements() {
		return uncommonElements;
	}

	public void setUncommonElements(List<Element> uncommonElements) {
		this.uncommonElements = uncommonElements;
	}
	
	public void tagDOM() {
		for (Element element : commonElements) {
			element.attr("style", "border: 1px dotted green");
		}
		for (Element element : uncommonElements) {
			element.attr("style", "border: 1px dotted red");
		}
	}

	public Document getUncommonTextDocument() {
		Document textDoc = Document.createShell("");
		textDoc.append("<html><head></head><body></body></html>");
		textDoc.body().html("<div id='post' style='border: 1px dotted black;'></div><div id='comments' style='border: 1px dotted red;'></div>");
		
		Element textBody = textDoc.getElementById("post");
		for (Element uncommonElement : uncommonElements) {
			String text = uncommonElement.ownText();
			if (!text.trim().isEmpty() && !commentElements.contains(uncommonElement)) {	
					TextNode tn = new TextNode(text, "");
					textBody.appendChild(tn);
					textBody.append("<br/>");
			}
		}
		
		
		String text = "Comments Count: ";
		if (commentsCountExact != null) {
			text += commentsCountExact;
		} else {
			text += "[" + commentsCountMin + " to " + commentsCountMax + "]";
		}
		
		TextNode tn = new TextNode(text, "");
		textBody.appendChild(tn);
		textBody.append("<br/>");
		
		
		
		Element commentsE = textDoc.getElementById("comments");
		
		for (Element element : orderedElements) {
			if (commentElements.contains(element)) {
				Element p = new Element(Tag.valueOf("p"), "");
				p.text(element.text());
				commentsE.appendChild(p);
			}
		}
		
		
		return textDoc;
	}
	public Document getUncommonDocument() {
		for (Element n : commonElements) {
			if (!n.nodeName().matches("html|head|body")) {
				if (n.nodeName().matches("script|style")) {
					n.remove();
				} else {
					n.unwrap();
				}
			}
		}
		return dom;
	}

	public List<Element> getCommentElements() {
		return commentElements;
	}

	public void setCommentElements(List<Element> commentElements) {
		this.commentElements = commentElements;
	}

	public List<Element> getOrderedElements() {
		return orderedElements;
	}

	public void setOrderedElements(List<Element> orderedElements) {
		this.orderedElements = orderedElements;
	}

	public Integer getCommentsCountExact() {
		return commentsCountExact;
	}

	public void setCommentsCountExact(Integer commentsCountExact) {
		this.commentsCountExact = commentsCountExact;
	}

	public Integer getCommentsCountMin() {
		return commentsCountMin;
	}

	public void setCommentsCountMin(Integer commentsCountMin) {
		this.commentsCountMin = commentsCountMin;
	}

	public Integer getCommentsCountMax() {
		return commentsCountMax;
	}

	public void setCommentsCountMax(Integer commentsCountMax) {
		this.commentsCountMax = commentsCountMax;
	}
}

package him.page;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PageAnalysisApplication {

    public static void main(String[] args) {
        SpringApplication.run(PageAnalysisApplication.class, args);
    }
}

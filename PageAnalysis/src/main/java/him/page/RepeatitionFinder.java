/**
 * 
 */
package him.page;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeVisitor;

/**
 * @author himanshu
 *
 */
public class RepeatitionFinder implements NodeVisitor {
	private HtmlPage page;
	private Map<NodeKey, List<Element>> depthNodes;
	public RepeatitionFinder(HtmlPage page) {
		this.page = page;
		depthNodes = new LinkedHashMap<NodeKey, List<Element>>();
	}
	/* (non-Javadoc)
	 * @see org.jsoup.select.NodeVisitor#head(org.jsoup.nodes.Node, int)
	 */
	@Override
	public void head(Node node, int depth) {
		if (node instanceof TextNode) {
			TextNode t = (TextNode) node;
			String text = t.text().trim();
			if (!text.isEmpty()) {
				int d2 = depth-1;
				Element element = (Element) t.parent();
				if (!page.getUncommonElements().contains(element)) 
					return;
					NodeKey nk = new NodeKey();
					nk.commonParent = ElementUtils.generatePath(element);
					nk.commonParentNode = element;
					nk.depth = d2;
					nk.nodeName = element.nodeName();
					nk.nodeClass = element.attr("class");
					if (!depthNodes.containsKey(nk)) {
						depthNodes.put(nk, new ArrayList<Element>());
					}
					if (!depthNodes.get(nk).contains(element))
						depthNodes.get(nk).add(element);
			}
		}

	}

	public Map<NodeKey, List<Element>> getDepthNodes() {
		return depthNodes;
	}
	/* (non-Javadoc)
	 * @see org.jsoup.select.NodeVisitor#tail(org.jsoup.nodes.Node, int)
	 */
	@Override
	public void tail(Node node, int depth) {
		// TODO Auto-generated method stub

	}

}

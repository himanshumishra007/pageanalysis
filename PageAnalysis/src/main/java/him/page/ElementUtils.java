/**
 * 
 */
package him.page;

import java.util.Iterator;

import org.jsoup.nodes.Element;

/**
 * @author himanshu
 *
 */
public class ElementUtils {
	public static String getSelectorPath(Element element) {
		return element.cssSelector();
	}
	public static String generatePath(Element toSearch) {
		Iterator<Element> e = toSearch.parents().iterator();
		
		String path = getNodeCss(toSearch);
		while (e.hasNext()) {
			Element n = e.next();
			path = getNodeCss(n) + " " + path;
		}
		return path;
	}
	
	public static String getNodeCss(Element n) {
		StringBuilder sb = new StringBuilder(n.nodeName());
		if (!n.id().trim().isEmpty()) {
			if (n.id().matches(".*[0-9]+")) {
				sb.append("[id*=").append(n.id().trim().replaceAll("[0-9]+", "")).append("]");
			} else {
				sb.append("#").append(n.id().trim());
			}
		}
		if (!n.attr("class").trim().isEmpty()) {
			String[] allClasses = n.attr("class").split("\\s+");
			String matchClass = "";
			for (String clas : allClasses) {
				if (clas.trim().isEmpty())
					continue;
				if (clas.matches(".*[0-9]+")) {
					matchClass += ("[class*=") + (clas.replaceAll("[0-9]+", "")) + ("]");
				} else {
					sb.append(".").append(clas);
				}
			}
			sb.append(matchClass);
		}
		return sb.toString();
	}
}

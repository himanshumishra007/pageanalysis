/**
 * 
 */
package him.page;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.springframework.stereotype.Service;

/**
 * @author himanshu
 *
 */
@Service
public class HtmlFetchService {
	private HttpClient client;
	private Map<String, String> cache = new HashMap<String, String>();
	
	@PostConstruct
	public void init() {
		client = new HttpClient();
	}
	
	public String fetchHtml(String url) throws HttpException, IOException {
		if (cache.containsKey(url)) 
			return cache.get(url);
		
		HttpMethod method = new GetMethod(url);
		client.executeMethod(method);
		String html = method.getResponseBodyAsString();
		cache.put(url, html);
		return html;
	}
}

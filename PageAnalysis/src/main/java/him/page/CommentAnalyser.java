/**
 * 
 */
package him.page;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;

import java.util.regex.Pattern;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author himanshu
 *
 */
public class CommentAnalyser {
	public static void analyse(HtmlPage page) {
		Integer xComments = checkXComments(page.getDom().text());
		if (xComments != null && xComments == 0) {
			page.setCommentsCountExact(0);
			return;
		}
		RepeatitionFinder repeatitionFinder = new RepeatitionFinder(page);
		page.getDom().traverse(repeatitionFinder);
		getCommentsCandidate(repeatitionFinder.getDepthNodes());
		List<Element> commentCandidates = groupComments(page, repeatitionFinder.getDepthNodes());
		page.setCommentElements(commentCandidates);
	}
	
	private static Integer checkXComments(String html) {
		Pattern xCommentsPattern = Pattern.compile("([0-9]+)\\s*comment", Pattern.CASE_INSENSITIVE);
		Matcher matcher = xCommentsPattern.matcher(html);
		Integer xComments = null;
		while (matcher.find()) {
			Integer newCount = Integer.parseInt(matcher.group(1));
			if (xComments == null) {
				xComments = newCount;
			} else if (xComments != newCount) {
				xComments = null;
				break;
			}
			
		}
		return xComments;
	}
	
	private static void getCommentsCandidate(Map<NodeKey, List<Element>> depthNodes) {
		boolean commentInKey = false;
		
		String commentRegex = ".*(comment|repl).*";
		for (Entry<NodeKey, List<Element>> depthNode : depthNodes.entrySet()) {
			if (depthNode.getKey().commonParent.matches(commentRegex)) {
				commentInKey = true;
				break;
			}
		}
		
		
		//Filter out singles
		Iterator<Entry<NodeKey, List<Element>>> eit = depthNodes.entrySet().iterator();
		while (eit.hasNext()) {
			Entry<NodeKey, List<Element>> entry = eit.next();
			if (entry.getValue().size() == 1) {
				eit.remove();
				continue;
			}
			if (commentInKey &&  !entry.getKey().commonParent.matches(commentRegex))
				eit.remove();
		}
		
		/*DO NOT DELETE
		 * for (Entry<NodeKey, List<Element>> depthNode : depthNodes.entrySet()) {
			Iterator<Element> it = depthNode.getValue().iterator();
			String t1 = it.next().text();
			boolean same = true;
			while (it.hasNext()) {
				Element n = (Element) it.next();
				if (!n.text().equals(t1)) {
					same = false;
					break;
				}
			}
			if (!same) {
				System.out.println(depthNode.getValue().size() + " " + depthNode.getKey());
				int counter = 0;
				for (Node n : depthNode.getValue()) {
					commentElements.add((Element)n);
					System.out.println(((Element)n).cssSelector());
					counter++;
				}
				System.out.println(depthNode.getValue().size() + "==" + counter + " (" + commentElements.size() + ")");
			}
			
		}*/
	}
	
	private static List<Element> groupComments(HtmlPage page, Map<NodeKey, List<Element>> depthNodes) {
		if (depthNodes.isEmpty())
			return new ArrayList<Element>();
		
		List<Element> uncommonElements = page.getUncommonElements();
		Map<NodeKey, Map<NodeKey, Integer>> order = new LinkedHashMap<NodeKey, Map<NodeKey, Integer>>();
		List<Set<NodeKey>> groupList = new ArrayList<Set<NodeKey>>();
		Map<NodeKey, AtomicInteger> currentGroup = new HashMap<NodeKey, AtomicInteger>();
		NodeKey prev = null;
		
		//Find comment group
		for (Element diffNode : uncommonElements) {
				for (Entry<NodeKey, List<Element>> entry : depthNodes.entrySet()) {
					if (entry.getValue().contains(diffNode)) {
						if (!currentGroup.containsKey(entry.getKey())) {
							currentGroup.put(entry.getKey(), new AtomicInteger(entry.getValue().size() - 1));
						} else {
							if (currentGroup.get(entry.getKey()).decrementAndGet() == 0) {
								boolean hasVal = false;
								for (AtomicInteger ai : currentGroup.values()) {
									if (ai.get() > 0) {
										hasVal = true;
										break;
									}
								}
								if (!hasVal) {
									groupList.add(currentGroup.keySet());
									currentGroup = new HashMap<NodeKey, AtomicInteger>();
								}
							}
						}
					}
				}
		}
		
		
		Set<NodeKey> commentGroup = groupList.iterator().next();
		for (Set<NodeKey> group : groupList) {
			if (group.size() > commentGroup.size()) {
				commentGroup = group;
			}
			for (NodeKey nk : group) {
				System.out.print(nk.nodeName + " " + nk.nodeClass + ", ");
			}
			System.out.println();
		}
		
		List<Element> commentElements = new ArrayList<Element>();
		
		depthNodes.keySet().retainAll(commentGroup);
		
		for (NodeKey nk : commentGroup) {
			Elements elements = page.getDom().select(nk.commonParent);
			depthNodes.get(nk).clear();
			depthNodes.get(nk).addAll(elements);
			for (Element n : elements) {
				commentElements.add(n);
			}
			//System.out.println(nk + " = " + nk.commonParentNode.cssSelector());
			System.out.println(nk + " = " + nk.commonParent);
			System.out.println(nk + " = " + elements.size());
		}
		
		
		
		
		for (Element e : page.getOrderedElements()) {
			for (NodeKey nk : commentGroup) {
				if (depthNodes.get(nk).contains(e)) {
					if (!order.containsKey(nk)) {
						order.put(nk, new LinkedHashMap<NodeKey, Integer>());
					}
					if (prev != null) {
						if (!order.get(prev).containsKey(nk)) {
							order.get(prev).put(nk, 1);
						} else {
							order.get(prev).put(nk, order.get(prev).get(nk) + 1);
						}
					}
					prev = nk;
				}
			}
		}
		
		if (commentElements.size() > 0) { 
			AtomicInteger minComments = new AtomicInteger();
			AtomicInteger maxComments = new AtomicInteger();
			Integer exactCount = guessNumberOfComments(minComments, maxComments, depthNodes, order);
			
			page.setCommentsCountExact(exactCount);
			page.setCommentsCountMax(maxComments.get());
			page.setCommentsCountMin(minComments.get());
		} else {
			page.setCommentsCountExact(0);
		}
		
		
		
		
		System.out.println(order);
		
		System.out.println(commentGroup);
		
		return commentElements;
	}

	private static Integer guessNumberOfComments(AtomicInteger minComments,
			AtomicInteger maxComments, Map<NodeKey, List<Element>> depthNodes, Map<NodeKey, Map<NodeKey, Integer>> order) {
		minComments.set(Integer.MAX_VALUE);
		maxComments.set(0);
		
		for (List<Element> e : depthNodes.values()) {
			if (minComments.get() > e.size())
				minComments.set(e.size());
			if (maxComments.get() < e.size())
				maxComments.set(e.size());
		}
		
		Set<NodeKey> nodesIgnoredForSize = new HashSet<NodeKey>();
		
		//in the group check if there are those who are child.. if true, set max as as min comments size and ignore them from size candidature
		for (NodeKey c1 : depthNodes.keySet()) {
			for (NodeKey c2 : depthNodes.keySet()) {
				if (c1 == c2)
					continue;
				if (c1.commonParent.startsWith(c2.commonParent)) {
					//C1 is child of C2
					
					nodesIgnoredForSize.add(c1);
					if (depthNodes.get(c1).size() < depthNodes.get(c2).size() && minComments.get() < depthNodes.get(c1).size()) {
						minComments.set(depthNodes.get(c1).size());
					}
					break;
				}
			}
		}
		
		//in order, if there is a repeatition, set min as max comments size and ignore them from size candidature
		for (NodeKey c1 : order.keySet()) {
			if (order.get(c1).containsKey(c1)) {
				nodesIgnoredForSize.add(c1);
				if (maxComments.get() > depthNodes.get(c1).size()) {
					maxComments.set(depthNodes.get(c1).size());
				}
			}
		}
		
		//From the remaining candidates, check if all have same value
		//I guess this is a guess TODO: Remove 
		Integer exactCount = null;
		for (NodeKey c1 : depthNodes.keySet()) {
			if (!nodesIgnoredForSize.contains(c1)) {
				if (exactCount == null) {
					exactCount = depthNodes.get(c1).size();
				} else if (exactCount != depthNodes.get(c1).size()) {
					exactCount = null;
					break;
				}
			}
		}
		
		
		
		if (minComments.get() == maxComments.get())
			exactCount = minComments.get();
		
		
		//x comments??
		return exactCount;
	}
	
}

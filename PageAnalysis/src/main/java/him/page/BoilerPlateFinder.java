/**
 * 
 */
package him.page;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.jsoup.select.NodeVisitor;

/**
 * @author himanshu
 *
 */
public class BoilerPlateFinder implements NodeVisitor {
	private List<Document> samplingDOMs;
	private List<Element> commonElements;
	private List<Element> uncommonElements;
	private List<Element> orderedElements;
	
	
	public BoilerPlateFinder(List<Document> samplingDOMs) {
		this.samplingDOMs = samplingDOMs;
		this.commonElements = new ArrayList<Element>();
		this.uncommonElements = new ArrayList<Element>();
		this.orderedElements = new ArrayList<Element>();
	}

	/**
	 *  If the node is an Element
	 *  
	 */
	@Override
	public void head(Node node, int depth) {
		if (node instanceof Element && !(node instanceof Document)) {
			Element element = (Element) node;
			
			String toMatchText = element.ownText().trim();
			String selector = ElementUtils.generatePath(element);
			boolean allDocCommon = true;
			
			String elementCss = ElementUtils.getNodeCss(element);
			for (Document doc : samplingDOMs) {
				Elements selectedElements = doc.select(selector);
				boolean foundCommon = false;
				for (int j = 0; j < selectedElements.size(); j++) {
					Element e2 = selectedElements.get(j);
					if (elementCss.equals(ElementUtils.getNodeCss(e2))) {
						String text = e2.ownText().trim();
						//TODO: Check and modify logic here... missing when text is not empty but toMatchText is
						if (!toMatchText.isEmpty()) {
							if (text.equals(toMatchText)) {
								foundCommon = true;
								break;
							}
						} else if (text.isEmpty()) {
							foundCommon = true;
							break;
						}
					}
				}
				if (!foundCommon) {
					allDocCommon = false;
				}
			}
			if (allDocCommon) {
				commonElements.add(element);
			} else {
				uncommonElements.add(element);
			}
			orderedElements.add(element);
		}
	}

	/** 
	 * NOP
	 */
	@Override
	public void tail(Node node, int depth) {
		
	}
	public List<Element> getCommonElements() {
		return commonElements;
	}
	public List<Element> getUncommonElements() {
		return uncommonElements;
	}

	public List<Element> getOrderedElements() {
		// TODO Auto-generated method stub
		return orderedElements;
	}
}

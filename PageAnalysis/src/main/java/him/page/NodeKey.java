/**
 * 
 */
package him.page;

import org.jsoup.nodes.Element;


/**
 * @author himanshu
 *
 */
public class NodeKey {
	int depth;
	public String commonParent;
	public String nodeName;
	public String nodeClass;
	public Element commonParentNode;
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commonParent == null) ? 0 : commonParent.hashCode());
		result = prime * result + depth;
		result = prime * result
				+ ((nodeClass == null) ? 0 : nodeClass.hashCode());
		result = prime * result
				+ ((nodeName == null) ? 0 : nodeName.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodeKey other = (NodeKey) obj;
		if (commonParent == null) {
			if (other.commonParent != null)
				return false;
		} else if (!commonParent.equals(other.commonParent))
			return false;
		if (depth != other.depth)
			return false;
		if (nodeClass == null) {
			if (other.nodeClass != null)
				return false;
		} else if (!nodeClass.equals(other.nodeClass))
			return false;
		if (nodeName == null) {
			if (other.nodeName != null)
				return false;
		} else if (!nodeName.equals(other.nodeName))
			return false;
		return true;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(depth + "-" +  nodeName + "." + nodeClass + " < " + commonParent);
		return String.valueOf(hashCode());
	}
}
